<?php get_header(); ?>

    <div class="row errorpage">
        <div class="small-12 columns">
            <h1><span>404</span></h1>
            <h3>Page Not Found</h3>
            <a href="<?php echo home_url(); ?>" class="button btn">Click here to return home</a>
        </div>
    </div>

<?php get_footer(); ?>
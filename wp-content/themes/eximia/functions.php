<?php
/**
 * General Wordpress Functions
 *
 * @since 1.0.0
 */
if (file_exists(dirname(__FILE__) . '/includes/functions/general.php')) {
    require_once(dirname(__FILE__) . '/includes/functions/general.php');
}

/**
 * Scripts
 *
 * @since 1.0.0
 */
if (file_exists(dirname(__FILE__) . '/includes/functions/scripts.php')) {
    require_once(dirname(__FILE__) . '/includes/functions/scripts.php');
}

/**
 * Editor / Tiny MCE
 *
 * @since 1.0.0
 */
if (file_exists(dirname(__FILE__) . '/includes/functions/editor.php')) {
    require_once(dirname(__FILE__) . '/includes/functions/editor.php');
}

/**
 * ACF Options
 *
 * @since 1.0.0
 */
if (file_exists(dirname(__FILE__) . '/includes/functions/acf.php')) {
    require_once(dirname(__FILE__) . '/includes/functions/acf.php');
}

/**
 * Menu Walkers
 *
 * @since 1.0.0
 */
/*if (file_exists(dirname(__FILE__) . '/includes/functions/walkers.php')) {
    require_once(dirname(__FILE__) . '/includes/functions/walkers.php');
}*/

/**
 * Custom Post Types
 *
 * @since 1.0.0
 */
if (file_exists(dirname(__FILE__) . '/includes/functions/custom-post-types.php')) {
    require_once(dirname(__FILE__) . '/includes/functions/custom-post-types.php');
}

/**
 * Include Admin Menu Walker.
 *
 * @since 1.0.0
 */
if (file_exists(dirname(__FILE__) . '/classes/SiteNav_Walker.php')) {
    require_once(dirname(__FILE__) . '/classes/SiteNav_Walker.php');
}

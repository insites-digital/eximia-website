<?php

if ($query->have_posts()) { ?>
<ul class="blog-posts" id="gallery" data-equalizer="foo">

            
         
	<?php
        while ($query->have_posts()) {
        $query->the_post();
        $perma_cat = get_post_meta('_category_permalink', true);
        if ($perma_cat != null) {
            $cat_id = $perma_cat['category'];
            $category = get_category($cat_id);
        } else {
            $categories = get_the_category();
            $category = $categories[0];
        }
        $category_name = $category->name;
        $cats = array();
        foreach (get_the_category() as $c) {
            $cat = get_category($c);
            array_push($cats, $cat->name);
        }
        if (sizeOf($cats) > 0) {
            $post_categories = implode(' ', $cats);
            $post_categories = str_replace($category_name, "", $post_categories);
        } else {
            $post_categories = '';
        }
    ?>

    

        <li class="small-12 medium-6 large-3 columns">
                <div class="blog-post" data-equalizer-watch="foo">
                <a href="<?php echo the_permalink(); ?>">

                <?php echo the_post_thumbnail('blog-listing'); ?>
                    <h4><?php echo the_title(); ?></h4>
                    </a>
                </div>
            </li>

    <?php } ?>
</ul>
<?php } else { global $searchandfilter; $sf_current_query = $searchandfilter->get(137)->current_query(); ?>


<?php } ?>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.matchHeight-min.js"></script>
		<script type="text/javascript">
(function ( $ ) {
	
	"use strict";

	$(function () {
        $(document).on("sf:ajaxfinish", ".searchandfilter", function(){
            $.fn.matchHeight._apply('.blog-post');
            // console.log("Javascript Fired.");
            $ .fn.matchHeight._update()
        });
        $( window ).resize(function() {
             $.fn.matchHeight._apply('.blog-post');
            // console.log("Javascript Fired.");
            $ .fn.matchHeight._update()
        });
	});

}(jQuery));
</script>
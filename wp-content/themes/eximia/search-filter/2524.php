<?php

/**
 * Search & Filter Pro
 *
 * Sample Results Template
 *
 * @package   Search_Filter
 * @author    Ross Morsali
 * @link      http://www.designsandcode.com/
 * @copyright 2015 Designs & Code
 *
 * Note: these templates are not full page templates, rather
 * just an encaspulation of the your results loop which should
 * be inserted in to other pages by using a shortcode - think
 * of it as a template part
 *
 * This template is an absolute base example showing you what
 * you can do, for more customisation see the WordPress docs
 * and using template tags -
 *
 * http://codex.wordpress.org/Template_Tags
 *
 */

if ($query->have_posts()) { ?>
<ul class="blog-posts" id="gallery" data-equalizer="foo">
<?php
while ($query->have_posts()) {
    $query->the_post();
    $perma_cat = get_post_meta('_category_permalink', true);
    if ($perma_cat != null) {
        $cat_id = $perma_cat['category'];
        $category = get_category($cat_id);
    } else {
        $categories = get_the_category();
        $category = $categories[0];
    }
    $category_name = $category->name;
    $cats = array();
    foreach (get_the_category() as $c) {
        $cat = get_category($c);
        array_push($cats, $cat->name);
    }
    if (sizeOf($cats) > 0) {
        $post_categories = implode(' ', $cats);
        $post_categories = str_replace($category_name, "", $post_categories);
    } else {
        $post_categories = '';
    }
    ?>

        <li class="small-12 medium-6 large-4 columns">
                <div class="blog-post" data-equalizer-watch="foo">
                <a href="<?php echo the_permalink(); ?>">

                <img src="<?php echo get_the_post_thumbnail_url(get_the_ID()); ?>" alt="">
                    <h4><?php echo the_title(); ?></h4>
                    </a>
                </div>
            </li>

    <?php 
} ?>
</ul>
<?php 
} else {
    global $searchandfilter;
    $sf_current_query = $searchandfilter->get(137)->current_query(); ?>
<?php 
} ?>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.matchHeight-min.js"></script>
		<script type="text/javascript">
(function ( $ ) {
	
	"use strict";

	$(function () {
        $(document).on("sf:ajaxfinish", ".searchandfilter", function(){
            $.fn.matchHeight._apply('.blog-post');
            // console.log("Javascript Fired.");
            $ .fn.matchHeight._update()
        });
        $( window ).resize(function() {
             $.fn.matchHeight._apply('.blog-post');
            // console.log("Javascript Fired.");
            $ .fn.matchHeight._update()
        });
	});

}(jQuery));
</script>
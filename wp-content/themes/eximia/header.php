<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
    <head>
        <meta charset="<?php bloginfo('charset'); ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
        <meta name="format-detection" content="telephone=no">
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>"/>
        <link rel="icon" type="image/png" href="<?php echo the_field('favicon', 'options'); ?>" />
        <!--[if lt IE 9]>
            <script src="<?php echo get_template_directory_uri(); ?>/assets/js/legacy.min.js"></script>
        <![endif]-->
        <?php wp_head(); ?>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <?php echo the_field('tracking_code', 'options'); ?>
    </head>

    <body <?php if (is_page()) { ?> id="<?php echo $post->post_name; ?>" <?php } ?>  <?php body_class(); ?>>

<script type="text/javascript" src="https://secure.ruth8badb.com/js/159163.js"></script>
<noscript><img src="https://secure.ruth8badb.com/159163.png" alt="" style="display:none;" /></noscript>
<?php include('partials/navigation.php') ?>
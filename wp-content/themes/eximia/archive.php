<?php get_header(); ?>




    <div class="row expanded filter-row">
        <div class="filter" id="gallery-menu">
            <img src="<?php echo the_field('filter_icon','options'); ?>" alt="">
            <a href="#" data-liquo="all">All</a>
            <?php 
    $categories = get_categories();
    foreach($categories as $category) {
       echo '<a href="#" data-liquo="' . $category->slug . '">' . $category->name . '</a>';
    }
    ?>
        </div>
    </div>

    <div class="row expanded blog-archive" data-equalizer="foo">
        <ul class="blog-posts" id="gallery">
            <?php 
            if (have_posts()) :
            while (have_posts()) :
            the_post();
            $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
            $categories = get_the_category();
            ?>
            <li class="small-12 medium-4 large-4 columns" data-liquo="<?php echo $categories[0]->slug; ?>">
                <div class="blog-post" data-equalizer-watch="foo">
            <a href="<?php echo the_permalink(); ?>">
                    <img src="<?php echo $image[0]; ?>" alt="">
                    <h4><?php echo the_title(); ?></h4>
            </a>
                </div>
            </li>
            <?php endwhile; ?> 
            <?php endif; ?>
            <?php wp_reset_postdata(); ?>
         </ul>
    </div>




<?php get_footer(); ?>
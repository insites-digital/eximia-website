<?php

class SiteNav_Walker extends Walker_Nav_Menu {

    public function start_lvl( &$output, $depth = 0, $args = [] ) {
        $indent = str_repeat( "\t", $depth );
        $output .= "\n$indent<ul role=\"menu\" class=\"dropdown-menu\">\n";
    }

    public function start_el( &$output, $item, $depth = 0, $args = [], $id = 0 ) {
        $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

        $classes     = [];
        $class_names = 'c-site-nav__item';

        if($args->has_children && $depth === 0) {
            $class_names .= ' has-dropdown';
        }

        if ( in_array( 'current-menu-item', $item->classes ) ) {
            $class_names .= ' active';
        }

        if ( in_array( 'admin-mobile', $item->classes )) {
            $class_names .= ' admin-mobile';
        }
        if (!empty($item->classes[0])) {
            $class_names .= ' ' . $item->classes[0];
        }
        $class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';
        $id = '';

        $output .= $indent . '<li' . $id . $class_names .'>';

        $atts           = [];
        $atts['title']  = ! empty( $item->title )   ? $item->title  : '';
        $atts['target'] = ! empty( $item->target )  ? $item->target : '';
        $atts['rel']    = ! empty( $item->xfn )     ? $item->xfn    : '';


        if ( $args->has_children ) {
            $atts['href']           = '#';
            $atts['data-toggle']    = 'dropdown';
            $atts['class']          = 'dropdown-toggle';
        }
        else {
            $atts['href'] = ! empty( $item->url ) ? $item->url : '';
        }

        $atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args );
        $attributes = '';

        foreach ( $atts as $attr => $value ) {
            if ( ! empty( $value ) ) {
                $value = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
                $attributes .= ' ' . $attr . '="' . $value . '"';
            }
        }

        $item_output = $args->before;

        $id = $item->classes[0];
        if (strpos($id, 'nav-btn') !== false) {
	        $item_output .= '<a id="nav-demo-btn" data-featherlight="#popup"'. $attributes .'>';
	      } else {
	        $item_output .= '<a'. $attributes .'>';
	      }


        $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
        $item_output .= ( $args->has_children && 0 === $depth ) ? ' <span class="c-icon-caret-highlight"></span></a>' : '</a>';
        $item_output .= $args->after;

        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
    }

    public function display_element( $element, &$children_elements, $max_depth, $depth, $args, &$output ) {
        if ( ! $element ) {
            return;
        }

        $id_field = $this->db_fields['id'];

        // Display this element.
        if ( is_object( $args[0] ) ) {
           $args[0]->has_children = ! empty( $children_elements[ $element->$id_field ] );
        }

        parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
    }
}

<?php get_header(); ?>

<div class="hero-banner diff-padd">
        <div class="row expanded">
            <div class="small-12 medium-6 columns padme">
                <h3>Insights</h3>
                <hr>
                <h4><?php echo get_the_author(); ?></h4>
                <p><?php echo the_author_meta('user_description'); ?></p>
                <a href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>" class="all-blogs">&laquo; Back to all insights</a>
            </div>
            <div class="small-12 medium-6 columns no-padding">
            <?php echo get_avatar( get_the_author_meta( 'ID' ), 850 ); ?>
            </div>
        </div>
    </div>

    <div class="row expanded blog-archive" data-equalizer="foo">
        <ul class="blog-posts" id="gallery">
            <?php 
            if (have_posts()) :
                while (have_posts()) :
                    the_post();
            $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
            $categories = get_the_category();

            ?>
            <li class="small-6 large-4 columns" data-liquo="<?php echo $categories[0]->slug; ?>">
                <div class="blog-post" data-equalizer-watch="foo">
                    <img src="<?php echo $image[0]; ?>" alt="">
                    <h4><a href="<?php echo the_permalink(); ?>"><?php echo the_title(); ?></a></h4>
                </div>
            </li>
            <?php endwhile; ?> 
            <?php endif; ?>
         </ul>
    </div>

<?php get_footer(); ?>
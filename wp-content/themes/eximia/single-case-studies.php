<?php get_header(); ?>
<?php get_template_part('/partials/flexible-content'); ?>

<br>

<?php get_footer(); ?>
<script>
function redirect(goto){
    if (goto != '') {
        window.location = goto;
    }
}

var selectEl = document.getElementById('explore');

selectEl.onchange = function(){
    var goto = this.value;
    redirect(goto);
    
};
</script>
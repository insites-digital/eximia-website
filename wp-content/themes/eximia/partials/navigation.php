<div class="row expanded menurow">
<nav class="c-site-nav">
    <div class="o-container c-site-nav__container">
        <a href="<?php echo home_url(); ?>" class="c-site-nav__home-link">
        <img src="<?php echo the_field('website_logo', 'options'); ?>" alt="<?php echo bloginfo('name'); ?>" class="wow fadeIn">
        </a>

        <?php wp_nav_menu([
            'theme_location' => 'main',
            'walker'     => '',
            'menu_class' => 'c-site-nav__list',
            'container'  => true,
            'walker' => new SiteNav_Walker(),
        ]); ?>

        <a href="#" class="c-menu-button js-menu-button">
            <span class="c-burger-icon"></span>
        </a>
    </div>
</nav>

</div>
<?php 
	$condition = get_field('open_closed');
?>

<div class="expanded row call-us-bar " id="callus">
	<div class="row">
		<div class="small-11 large-8 p-0 m-0 columns">
			<span><i class="material-icons phone">phone</i> Call us on 020 7420 1984 or send us a message</span>
		</div>
		<div class="small-1 large-4 p-0 m-0 columns right">
			<a href="#callus" id="slide-down-contact">
			<i class="material-icons arrow">expand_more</i>
			</a>
		</div>
	</div>
	
</div>

<div class="row" id="cform" <?php if ($condition == "open"): ?>
	style="display:block;"
<?php endif ?>>
	<?php echo do_shortcode('[contact-form-7 id="6" title="Call Us Bar Form"]'); ?>
</div>


<div class="row">
    <div class="small-12 columns">
        <div id="mc_embed_signup">
            <form action="https://eximiadesign.us3.list-manage.com/subscribe/post?u=31c428d74a79e4cc02cbe10c7&amp;id=5530d9d76a" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                <div id="mc_embed_signup_scroll">
                    <div class="mc-field-group">
                        <input type="text" value="" name="FNAME" placeholder="Name" class="" id="mce-FNAME">
                        <input type="email" placeholder="Email*" value="" name="EMAIL" class="required email" id="mce-EMAIL">
                    </div>
                    <br>
                    <div class="mc-field-group input-group">
                        <h6>Key areas of interest</h6>
                        <ul>
                            <li><input type="checkbox" value="1" name="group[23145][1]" id="mce-group[23145]-23145-0"><label for="mce-group[23145]-23145-0">Governance, risk and compliance</label></li>
                            <li><input type="checkbox" value="2" name="group[23145][2]" id="mce-group[23145]-23145-1"><label for="mce-group[23145]-23145-1">Reward and share plans</label></li>
                            <li><input type="checkbox" value="4" name="group[23145][4]" id="mce-group[23145]-23145-2"><label for="mce-group[23145]-23145-2">Strategy, brand and values</label></li>
                            <li><input type="checkbox" value="8" name="group[23145][8]" id="mce-group[23145]-23145-3"><label for="mce-group[23145]-23145-3">People strategy and development</label></li>
                        </ul>
                    </div>
                    <div class="mc-field-group input-group">
                        <p>Increase the effectiveness of your colleague communications by receiving tips, ideas, trends, events and exclusives. We focus on quality over quantity so will never overwhelm your inbox, but you can easily unsubscribe at any time.</p>
                        <ul>
                            <li>
                                <input type="checkbox" style="float:left;display:inline-block;" value="16" name="group[23149][16]" id="mce-group[23149]-23149-0">
                                 <label for="mce-group[23149]-23149-0" style="display: inline-block;width: 80%;margin-top: -6px;">I’m happy for my details above to be used so I can receive email newsletters and relevant industry updates from Eximia.</label>
                            </li>
                        </ul>
                    </div>
                    </div>
                    <div class="mc-field-group input-group">
                    <?php 
                    $privacy_policy_page = get_option('wp_page_for_privacy_policy');

                    if ($privacy_policy_page) {
                        $permalink = esc_url(get_permalink($privacy_policy_page));
                    }
                     ?>
                    <p>By clicking below, you consent that we may process your information in accordance with our ​<a href="<?php echo $permalink ?>" target="_blank" title="Privacy Policy">Privacy policy​</a>.</p>
                    </div>
                    <div id="mce-responses" class="clear">
                        <div class="response" id="mce-error-response" style="display:none;visibility: hidden;"></div>
                        <div class="response" id="mce-success-response" style="display:none;visibility: hidden;"></div>
                    </div>
                    <div style="position: absolute; left: -5000px;" aria-hidden="true">
                        <input type="text" name="b_31c428d74a79e4cc02cbe10c7_5530d9d76a" tabindex="-1" value="">
                    </div>
                    <div class="clear">
                        <input type="submit" value="Submit" name="subscribe" id="mc-embedded-subscribe" class="button right mcbutton">
                    </div>
                </div>
            </form>
        </div>
        <script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';fnames[3]='ADDRESS';ftypes[3]='address';fnames[4]='PHONE';ftypes[4]='phone';}(jQuery));var $mcj = jQuery.noConflict(true);</script>

        
    </div>
</div>
<?php
    if( get_field('content_layout') ):
    while ( has_sub_field('content_layout') ) :
    if( get_row_layout() == 'hero_banner_-_stacked' ):
        get_template_part('partials/flexible-content/hero_banner-stacked');
    elseif( get_row_layout() == 'hero_banner_text_left' ): 
        get_template_part('partials/flexible-content/hero_banner-text-left');
    elseif( get_row_layout() == 'hero_banner_case_study' ): 
        get_template_part('partials/flexible-content/hero_banner-case-study');
    elseif( get_row_layout() == 'hero_banner_text_right' ): 
        get_template_part('partials/flexible-content/hero_banner-text-right');
    elseif( get_row_layout() == 'full_bleed_image_with_text' ): 
        get_template_part('partials/flexible-content/full_bleed_images');
    elseif( get_row_layout() == 'standard_image_with_text' ): 
        get_template_part('partials/flexible-content/standard-image-text');
    elseif( get_row_layout() == 'clients_carousel' ): 
        get_template_part('partials/flexible-content/clients_carousel');
    elseif( get_row_layout() == 'image_slider' ):
        get_template_part('partials/flexible-content/image_slider');
    elseif( get_row_layout() == 'testimonial_section' ): 
        get_template_part('partials/flexible-content/testimonial_section');
    elseif( get_row_layout() == 'latest_blogs' ): 
        get_template_part('partials/flexible-content/latest_blogs');
    elseif( get_row_layout() == 'video_section' ): 
        get_template_part('partials/flexible-content/video_section');
    elseif( get_row_layout() == 'team_image_with_text' ): 
        get_template_part('partials/flexible-content/team_image_with_text');
    elseif( get_row_layout() == 'icon_grid' ):
        get_template_part('partials/flexible-content/icon_grid');
    elseif( get_row_layout() == 'large_text_block' ):
        get_template_part('partials/flexible-content/large-text-block');
    elseif( get_row_layout() == 'standard_text_block' ):
        get_template_part('partials/flexible-content/standard_text_block');
    elseif( get_row_layout() == 'two_column_text_block' ):
        get_template_part('partials/flexible-content/two_col_text_block');
    elseif( get_row_layout() == 'indented_text_block'):
        get_template_part('partials/flexible-content/indented_text_block');
    elseif( get_row_layout() == 'contact_form'):
        get_template_part('partials/flexible-content/contact_form');
    elseif( get_row_layout() == 'map'):
        get_template_part('partials/flexible-content/map');
    elseif( get_row_layout() == 'tabs'):
        get_template_part('partials/flexible-content/tabs');
    elseif( get_row_layout() == 'tabs_var'):
        get_template_part('partials/flexible-content/tabs_variant');
    elseif( get_row_layout() == 'accred_carousel'):
        get_template_part('partials/flexible-content/accred_carousel');
    endif;
    endwhile;
    endif;
?>
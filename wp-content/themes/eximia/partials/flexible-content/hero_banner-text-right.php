<div class="hero-banner cstudy diff-padd">
    <div class="row expanded flip">
        <div class="small-12 medium-12 columns no-padding wow fadeIn">
            <img src="<?php echo the_sub_field('banner_image'); ?>" alt="">
        </div>
        <div class="small-12 medium-12 large-4 large-offset-2 large-pull-1 columns padme wow fadeIn textcol">
            <h1><?php echo the_sub_field('banner_title'); ?></h1>
            <hr>
            <?php echo the_sub_field('banner_description'); ?>
        </div>
    </div>
</div>
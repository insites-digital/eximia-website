<div class="row">
    <div class="small-12 columns standard-tb wow fadeIn">
        <div style="text-align: <?php echo the_sub_field('text_alignment'); ?>">
            <?php echo the_sub_field('text_content'); ?>
        </div>
    </div>
</div>
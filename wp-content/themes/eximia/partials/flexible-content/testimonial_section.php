<?php 
$posts = get_sub_field('testimonial_to_display'); 
if ($posts): 
?>


    <div class="testimonial-section">
        <div class="row">
            
            <div class="previous-testimonial hide-for-small-only">
                <a class="previous-testimonial-link">
                    <svg width="31px" height="60px" viewBox="0 0 31 60" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="square">
                            <g id="Eximia---Home" transform="translate(-49.000000, -3812.000000)" fill-rule="nonzero" stroke="#7A776C">
                                <g id="Group-10" transform="translate(50.000000, 3812.000000)">
                                    <polyline id="Line" points="28.8024948 59.7291886 0.0330983205 30.2027027 28.8024948 0.0576171875"></polyline>
                                </g>
                            </g>
                        </g>
                    </svg>
                </a>
            </div>
            <div class="next-testimonial hide-for-small-only">
                <a class="next-testimonial-link">
                    <svg width="31px" height="60px" viewBox="0 0 31 60" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="square">
                            <g id="Eximia---Home" transform="translate(-1360.000000, -3812.000000)" fill-rule="nonzero" stroke="#7A776C">
                                <g id="Group-10-Copy" transform="translate(1375.500000, 3842.000000) rotate(180.000000) translate(-1375.500000, -3842.000000) translate(1361.000000, 3812.000000)">
                                    <polyline id="Line" points="28.8024948 59.7291886 0.0330983205 30.2027027 28.8024948 0.0576171875"></polyline>
                                </g>
                            </g>
                        </g>
                    </svg>
                </a>
            </div>
            
            <div class="medium-8 medium-offset-4 medium-pull-2 small-12 columns wow fadeIn">
                <div class="autoplay-testimonials">
                    <?php foreach ($posts as $post) : ?>
                    <?php setup_postdata($post); ?>
                    <div>
                        <?php echo the_content(); ?>
                        <h4><?php echo the_field('testimonial_job_role'); ?></h4>
                        <h4><?php echo the_field('testimonial_company'); ?></h4>
                    </div>
                    <?php endforeach; ?>
                    <?php wp_reset_postdata(); ?>
                </div>
            </div>
            
            
        </div>
    </div>
<?php endif; ?>
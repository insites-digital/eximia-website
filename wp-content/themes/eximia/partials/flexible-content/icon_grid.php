<div class="row icon-grid" data-equalizer="foo">
		<?php 
		$title = get_sub_field('section_title'); 
		if($title):
		?>
    <div class="small-12 medium-10 medium-offset-2 medium-pull-1 large-9 large-offset-2 large-pull-1 columns wow fadeIn">
        <h3><?php echo the_sub_field('section_title'); ?></h3>
    </div>
		<?php endif; ?>
    <?php if( have_rows('icons') ): ?>
    <div class="small-12 large-9 medium-10 medium-offset-2 medium-pull-1 large-offset-3 large-pull-1 columns wow fadeIn">
        <?php while( have_rows('icons') ): the_row(); 
        $icon = get_sub_field('icon');
        $title = get_sub_field('title');
        $text = get_sub_field('text');
        ?>
        <div class="small-12 medium-4 medium-offset-0 medium-pull-0 large-3 large-offset-1 large-pull-1 columns end wow fadeIn iconbox">
            <img src="<?php echo $icon; ?>" alt="" data-equalizer-watch="foo">
            <h3 class="icon-title"><?php echo $title; ?></h3>
            <p><?php echo $text; ?></p>
        </div>
        

        <?php endwhile; ?>
        
    <?php endif; ?>
        </div>
    </div>
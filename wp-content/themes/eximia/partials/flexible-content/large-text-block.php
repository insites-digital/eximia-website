<div class="row">
    <div class="small-12 medium-10 medium-offset-2 medium-pull-1 columns large-tb wow fadeIn">
        <div style="text-align: <?php echo the_sub_field('text_alignment'); ?>">
            <?php echo the_sub_field('text_content'); ?>
        </div>
    </div>
</div>
<?php
  $condition = get_sub_field('image_left_or_right');
?>

    <?php if($condition == 'left') { ?>
        <div class="row team-member">
            <div class="large-offset-1 large-5 medium-6 small-12 columns wow fadeIn">
                <img src="<?php echo the_sub_field('team_member_image'); ?>" class="tmimg" alt="">
                <p class="team-name">
                    <?php echo the_sub_field('team_member_name') ?>
                </p>
                <p class="job-role">
                    <?php echo the_sub_field('team_member_job_role'); ?>
                </p>
                <a target="_blank" href="<?php echo the_sub_field('linkedin_url'); ?>">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/linkedin.svg" class="li-icon" alt="Linkedin"> Connect with me on LinkedIn
                </a>
            </div>
            <div class="large-offset-1 large-5 medium-6 small-12 columns wow fadeIn">
                <?php echo the_sub_field('text_area'); ?>
            </div>
        </div>
    <?php } else { ?>
        <div class="row team-member">
            <div class="large-6 columns  wow fadeIn">
                <?php echo the_sub_field('text_area'); ?>
            </div>
            <div class="large-6 columns wow fadeIn">
                <img src="<?php echo the_sub_field('team_member_image'); ?>" class="tmimg" alt="">
                <p class="team-name">
                    <?php echo the_sub_field('team_member_name') ?>
                </p>
                <p class="job-role">
                    <?php echo the_sub_field('team_member_job_role'); ?>
                </p>
                <a target="_blank" href="<?php echo the_sub_field('linkedin_url'); ?>"><i class="fa fa-linkedin"></i> Connect with me on linkedin</a>
            </div>
        </div>
    <?php } ?>

<?php $conditionleft = get_sub_field('image_side'); ?>
<?php if ($conditionleft == 'left') { ?>
    <div class="row expanded text-right-with-image full-bleed wow fadeIn">
        <div class="small-12 medium-12 large-6 columns pull-left">
            <img src="<?php echo the_sub_field('image') ?>" alt="">
        </div>
        <div class="small-12 medium-12 large-4 large-offset-2 large-pull-2 columns no-mobile-padding">
            <?php echo the_sub_field('text_area') ?>
            <a href="<?php echo the_sub_field('button_link') ?>" class="button"><?php echo the_sub_field('button_text'); ?></a>
        </div>
    </div>
<?php } else if ($conditionleft == 'right') { ?>
    <div class="row expanded text-left-with-image full-bleed wow fadeIn">
        <div class="small-12 medium-12 large-4 large-offset-1 columns">
            <?php echo the_sub_field('text_area') ?>
            <a href="<?php echo the_sub_field('button_link') ?>" class="button"><?php echo the_sub_field('button_text'); ?></a>
        </div>
        <div class="small-12 medium-12 large-6 columns no-padding">
            <img src="<?php echo the_sub_field('image') ?>" alt="" class="no-padding">
        </div>                    
    </div>
<?php } ?>
<div class="clients-carousel wow fadeIn">
    <div class="row">
      <div class="large-11 large-offset-1 columns">
        <h3><?php echo the_sub_field('section_title'); ?></h3>
      </div>
    </div>
    <?php 
    $posts = get_sub_field('logos_to_show');
    if ($posts) : ?>
    <div class="row">
      <div class="autoplay-clients">
      <?php foreach ($posts as $p) : // variable must NOT be called $post (IMPORTANT) ?>
      <?php $url = wp_get_attachment_image_src(get_post_thumbnail_id($p->ID), 'client-logo'); ?>
	      <div>
          <img src="<?php echo $url[0]; ?>" alt="">
	      </div>
      <?php endforeach; ?>
      </div>
    </div>
    <?php endif; ?>
</div>
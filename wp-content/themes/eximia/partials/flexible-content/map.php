 <?php
 $condition = get_sub_field('map_left_or_right');
    $location = get_sub_field('map');
?>


<style type="text/css">

.acf-map {
	width: 100%;
	height: 530px;
}

/* fixes potential theme css conflict */
.acf-map img {
   max-width: inherit !important;
}

</style>


<?php if ($condition == 'left') { ?>
    <section class="fw-map">
        <div class="row expanded">
            <div class="small-12 medium-6 columns no-padding">
            <div class="acf-map wow fadeIn">
                <div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
            </div>
            </div>
            <div class="small-12 medium-6 columns wow fadeIn">
                <?php echo the_sub_field('map_title'); ?>
            </div>
        </div>
    </section>
<?php } else { ?>
    <section class="fw-map">
        <div class="row expanded">
            <div class="small-12 medium-6 columns wow fadeIn">
                <?php echo the_sub_field('map_title'); ?>
            </div>
            <div class="small-12 medium-6 columns no-padding wow fadeIn">
            <div class="acf-map">
                <div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
            </div>
            </div>
        </div>
    </section>
<?php } ?>
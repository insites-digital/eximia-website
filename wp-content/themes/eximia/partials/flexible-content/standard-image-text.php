<?php
    $conditionleft = get_sub_field('image_side');
?>

    <?php if ($conditionleft == 'left') { ?>
        <div class="row text-right-with-image wow fadeIn">
            <div class="small-12 medium-12 large-7 columns pad-left">
                <img src="<?php echo the_sub_field('image') ?>" alt="">
            </div>
            <div class="small-12 medium-12 large-5 columns pad-right">
                <?php echo the_sub_field('text_area') ?>
                <a href="<?php echo the_sub_field('button_link') ?>" class="button"><?php echo the_sub_field('button_text'); ?></a>
            </div>
        </div>
    <?php } else if ($conditionleft == 'right') { ?>
        <div class="row text-left-with-image wow fadeIn">
            <div class="small-12 medium-12 large-5 columns pad-left">
                <?php echo the_sub_field('text_area') ?>
                <a href="<?php echo the_sub_field('button_link') ?>" class="button"><?php echo the_sub_field('button_text'); ?></a>
            </div>
            <div class="small-12 medium-12 large-7 columns">
                <img src="<?php echo the_sub_field('image') ?>" alt="">
            </div>                    
        </div>
    <?php } ?>
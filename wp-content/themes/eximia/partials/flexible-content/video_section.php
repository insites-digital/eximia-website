<?php
 $condition = get_sub_field('video_type');
?>

    <?php if($condition == 'fullwidth') { ?>
        <div class="row expanded full-width-video">
            <div class="embed-container wow fadeIn">
                <?php echo the_sub_field('video_embed'); ?>
            </div>
        </div>
        <?php } else { ?>
        <div class="row video-row">
            <div class="large-12 columns standard-video embed-container wow fadeIn">
                <?php echo the_sub_field('video_embed'); ?>
            </div>
        </div>
    <?php } ?>

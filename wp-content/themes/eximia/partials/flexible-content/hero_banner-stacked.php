<div class="hero-banner wow fadeIn">
    <div class="row">
        <div class="large-12 columns">
            <?php echo get_sub_field('banner_text'); ?>
            <img src="<?php echo get_sub_field('banner_image'); ?>" alt="Hero Example">
        </div>
    </div>
</div>
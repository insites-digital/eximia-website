<div class="carousel">
    <div class="row carousel-row">
        <div class="autoplay-imagegallery" data-equalizer>
            <?php while ( have_rows('images') ) : the_row(); ?>
            <div>
                <img src="<?php echo the_sub_field('image'); ?>" alt="">
            </div>
            <?php endwhile; ?>
        </div>
    </div>
</div>
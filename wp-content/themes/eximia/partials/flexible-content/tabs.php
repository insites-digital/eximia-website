<div class="row expanded tabs tabrow wow fadeIn normal-tabs">
    <?php if (have_rows('tabs')) : ?>
        <div class="row tabs" data-deep-link="true" data-update-history="true" data-deep-link-smudge="true" data-deep-link-smudge-delay="500" data-tabs id="deeplinked-tabs" data-equalizer>
            <ul class="tabs fontsizes" data-responsive-accordion-tabs="tabs small-accordion large-tabs" id="example-tabs">
                <?php $count = 0; ?>
                <?php while (have_rows('tabs')) : the_row(); ?>
                <?php $svg = get_sub_field('tab_icon'); ?>
                                <li class="tabs-title large-3 small-12 columns <?php if ($count == 0) {
                                                                                    echo "is-active";
                                                                                } ?>" data-equalizer-watch>

                    <a href="#panel<?php echo $count; ?>" aria-selected="<?php if ($count == 1) { echo "true"; } ?>">
                        <?php echo file_get_contents($svg); ?>
                        <h4><?php echo the_sub_field('tab_title'); ?></h4>
                    </a>
                </li>
                <?php $count++; ?>
                <?php endwhile; ?>
            </ul>
        </div>
    <?php endif; ?>
</div>


<?php if (have_rows('tabs')) : ?>
    <div class="tabs-content normal-tabs-content" data-tabs-content="example-tabs">
        <?php $count = 0; ?>
        <?php while (have_rows('tabs')) : the_row(); ?>
        <?php $svg = get_sub_field('tab_icon'); ?>
        <div class="tabs-panel <?php if ($count == 0) {
                                    echo "is-active";
                                } ?>" id="panel<?php echo $count; ?>">
            <div class="row">
                <div class="small-12 medium-12 large-6 pull-left columns">
                    <img src="<?php echo the_sub_field('in-tab_image'); ?>" alt="<?php echo the_sub_field('tab_title'); ?>">
                </div>
                <div class="small-12 medium-12 large-6 columns">
                    <h4><?php echo the_sub_field('tab_title'); ?></h4>
                    <?php echo the_sub_field('tab_content'); ?>
                    <a href="<?php echo the_sub_field('tab_button_link'); ?>" class="button"><?php echo the_sub_field('tab_button_text'); ?></a>
                </div>
            </div>
        </div>
        <?php $count++; ?>
        <?php endwhile; ?>
    </div>
<?php endif; ?>
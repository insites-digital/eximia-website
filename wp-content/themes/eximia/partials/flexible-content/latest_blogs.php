 <div class="row expanded big-pad" data-equalizer="foo">
        <div class="blog-posts">
            <h3><?php echo the_sub_field('section_title'); ?></h3>
            <?php 
                $args = array( 
                'post_type' => 'post',
                'posts_per_page' => 4,
                'post__not_in' => array($currentID)
                );
                $the_query = new WP_Query( $args );
            ?>

            <?php 
            if ( $the_query->have_posts() ) :
            while ( $the_query->have_posts() ) : 
            
            $the_query->the_post(); 
            $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'blog-listing' );
            ?>
            <div class="small-12 medium-3 large-3 columns wow fadeIn">
                <div class="blog-post" data-equalizer-watch="foo">
                    <img src="<?php echo $image[0]; ?>" alt="">
                    <h4><a href="<?php echo the_permalink(); ?>"><?php echo the_title(); ?></a></h4>
                </div>
            </div>
            <?php endwhile; ?> 
            <?php endif; ?>
            <?php wp_reset_query(); ?>
        </div>
    </div>
    <div class="row readmoreblogs">
      <div class="columns small-8 medium-6 large-3 small-centered wow fadeIn">
          <a href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>" class="button all-blogs">Read more</a>
        </div>
    </div>
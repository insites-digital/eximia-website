<div class="row id-tb">
    <div class="small-12 medium-2 medium-offset-2 medium-pull-1 columns wow fadeIn">
        <h4><?php echo the_sub_field('title_text') ?></h4>
    </div>
    <div class="small-12 medium-6 medium-pull-2 columns wow fadeIn">
        <?php echo the_sub_field('text_content') ?>
    </div>
</div>
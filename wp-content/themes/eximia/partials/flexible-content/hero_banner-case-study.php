<div class="hero-banner cstudy diff-padd" >
    <div class="row expanded" data-equalizer>
        <div class="small-12 medium-12 large-4 large-offset-2 large-pull-1 columns padme textcol" data-equalizer-watch>
            <img id="clientlogo" src="<?php echo the_sub_field('client_logo'); ?>" alt="">
            <hr>
            <?php echo the_sub_field('banner_description'); ?>
        </div>
        <div class="small-12 medium-12 large-6 columns no-padding" data-equalizer-watch>
            <img src="<?php echo the_sub_field('banner_image'); ?>" alt="">
        </div>
    </div>
</div>
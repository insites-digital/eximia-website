<div class="reveal" id="subscribe-modal" data-reveal>
    <div class="row">
        <div class="small-1 columns">
            <i class="material-icons pink">email</i>
        </div>
        <div class="small-9 columns">
            <h3>Subscribe for insights and updates</h3>
        </div>
        <div class="small-2 right columns">
            <i class="material-icons close" data-close>close</i>
        </div>
    </div>
    <?php get_template_part('partials/mailchimp-form'); ?>
</div>
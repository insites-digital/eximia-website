<?php
    if( get_field('layouts') ):
    while ( has_sub_field('layouts') ) :
    if( get_row_layout() == 'standard_text_area' ): 
?>
    <div class="row single-post-content">
        <div class="large-6 large-offset-6 large-pull-3 columns">
            <?php echo the_sub_field('text'); ?>
        </div>
    </div>
<?php
    elseif( get_row_layout() == 'testimonial' ):
    $posts = get_sub_field ('testimonial');
    if ($posts) :
?>
    <div class="testimonial-section">
        <div class="row">
            <div class="medium-6 medium-offset-6 medium-pull-3 small-12 columns wow fadeIn">
                <div class="autoplay-testimonials">
                    <?php foreach ($posts as $post) : ?>
                    <?php setup_postdata($post); ?>
                    <div>
                        <?php echo the_content(); ?>
                    </div>
                    <?php endforeach; ?>
                    <?php wp_reset_postdata(); ?>
                </div>
            </div>
            
            
        </div>
    </div>
<?php endif; ?>

<?php
    elseif( get_row_layout() == 'text_with_border_left' ):  ?>

        <div class="row single-post-content blpr">
            <div class="large-9 large-offset-4 large-pull-3 columns blp">
                <?php echo the_sub_field('text_area'); ?>
            </div>
        </div>
 
<?php
endif;
endwhile;
endif;
?>
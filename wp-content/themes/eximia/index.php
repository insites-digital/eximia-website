<?php get_header(); ?>

    <div class="row expanded filter-row">
        <div class="before-filter">
            <img src="<?php echo the_field('filter_icon', 'options'); ?>" alt="">
        </div>
        <div class="filter">
            <?php echo do_shortcode('[searchandfilter id="137"]'); ?>
        </div>
    </div>

    <div class="row expanded" style="padding: 0 20px 40px 0px">
        <?php echo do_shortcode('[searchandfilter id="137" show="results"]'); ?>        
    </div>

<?php get_footer(); ?>
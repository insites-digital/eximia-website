	<?php echo get_template_part('partials/call-us-bar') ?>

	<footer class="footer" style="background: <?php echo the_field('footer_background_colour'); ?>;">
		<div class="row wow fadeIn">
			<div class="large-2 columns custom-width show-for-large">
				<div class="row">
					<?php dynamic_sidebar('footer-column-1'); ?>
				</div>
			</div>
			<div class="large-2  custom-width-2 p-0 m-0 show-for-large columns">
			<?php dynamic_sidebar('footer-column-2'); ?>
			</div>
			<div class="large-2  p-0 m-0 small-6 columns">
			<?php dynamic_sidebar('footer-column-3'); ?>
			</div>
			<div class="large-5 follow-col p-0 m-0 small-6 columns">
				<h5>
					Follow us
				</h5>
			<ul class="social-links">
			<?php if (get_field('linkedin_url', 'options')) { ?>
				<li><a target="_blank" href="<?php echo the_field('linkedin_url', 'options'); ?>"><i class="fa fa-linkedin"></i></a></li>
			<?php 
	} ?>
			<?php if (get_field('facebook_url', 'options')) { ?>
				<li><a target="_blank" href="<?php echo the_field('facebook_url', 'options'); ?>"><i class="fa fa-facebook"></i></a></li>
			<?php } ?>

			<?php if (get_field('twitter_url', 'options')) { ?>
				<li><a target="_blank" href="<?php echo the_field('twitter_url', 'options'); ?>"><i class="fa fa-twitter"></i></a></li>
			<?php } ?>

			<?php if (get_field('instagram_url', 'options')) { ?>
				<li><a target="_blank" href="<?php echo the_field('instagram_url', 'options'); ?>"><i class="fa fa-instagram"></i></a></li>
			<?php } ?>
			</ul>
			<br>
			<a class="button show-for-large" id="subbut" href="#subbut" data-open="subscribe-modal"><i class="material-icons">email</i> Click to subscribe for insights and updates.</a>
				<?php dynamic_sidebar('footer-column-4'); ?>
			</div>
		</div>
		<div class="row bottom-footer wow fadeIn">
			<hr>
			<div class="large-7 columns small-12  show-for-large">
			&copy; <?php echo date('Y') ?> <?php echo the_field('copyright_text', 'options'); ?>

			<?php 
				$defaults = array(
					'theme_location' => 'policies',
					'container' => false,
					'echo' => true,
					'items_wrap' => '%3$s',
					'depth' => 0,
				);
			?>

			<?php wp_nav_menu($defaults); ?>
			</div>
			<div class="small-12 columns hide-for-large">
			&copy; <?php echo date('Y'); echo " "; echo the_field('copyright_text', 'options'); ?> 
			<br>
			</div>
			<div class="large-5 small-12 columns">
			<?php echo the_field('legal_text', 'options'); ?>
			</div>
			
			<div class="small-12 columns  hide-for-large">
			<?php 
				$defaults = array(
					'theme_location' => 'policies',
					'container' => false,
					'echo' => true,
					'items_wrap' => '%3$s',
					'depth' => 0,
				);
			?>
			<?php wp_nav_menu($defaults); ?>
			</div>
		</div>
	</footer>

	<?php echo get_template_part('partials/modal') ?>
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<?php wp_footer(); ?>

	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBDmuAtj9XAqCF2B7b4ZwZsy9X0-AXcF8c"></script>
<script>
$(document).ready(function () {
  var windowWidth = $(window).width();
  if (windowWidth <= 1024) { //for iPad & smaller devices
    $('.accordion-item').removeClass('is-active');
    $('.tabs-panel').removeClass('is-active');
	$('.accordion-content').css('display', 'none');
  }
});
</script>
    </body>
</html>

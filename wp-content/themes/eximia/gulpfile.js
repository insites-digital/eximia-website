/*******************************************************************************/
/* GULP SETUP
/*******************************************************************************/

var gulp            = require('gulp'),
    concat          = require('gulp-concat'),
    rename          = require('gulp-rename'),
    uglify          = require('gulp-uglify'),
    cleanCSS        = require('gulp-clean-css'),
    eslint          = require('gulp-eslint'),
    SASS            = require('gulp-sass'),
    babel           = require('gulp-babel'),
    sourcemaps      = require('gulp-sourcemaps'),
    imagemin        = require('gulp-imagemin'),
    pngquant        = require('imagemin-pngquant'),
    autoprefixer    = require('gulp-autoprefixer'),
    plumber         = require('gulp-plumber'),
    liveReload      = require('gulp-livereload'),
    markdown        = require('gulp-markdown'),
    fileinclude     = require('gulp-file-include'),
    gutil           = require('gulp-util'),
    ftp             = require('vinyl-ftp'),
    browserSync     = require('browser-sync').create(),
    watch           = require('gulp-watch');

    const notifier  = require('node-notifier');

gulp.task('concat', function() {
    gulp.src(['resources/js/vendor/*.js', '!resources/js/vendor/modernizer.js'])
        .pipe(concat('plugin.js'))
        .on('error', swallowError)
        .pipe(uglify())
        .on('error', swallowError)
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('assets/js/'));

    gulp.src('resources/js/legacy/*.js')
        .pipe(concat('legacy.js'))
        .on('error', swallowError)
        .pipe(uglify())
        .on('error', swallowError)
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('assets/js/'));

    gulp.src(['resources/sass/vendor/*.css'])
        .pipe(concat('vendor.css'))
        .on('error', swallowError)
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .on('error', swallowError)
        .pipe(rename({suffix: '.min'}))
        .on('error', swallowError)
        .pipe(gulp.dest('assets/css/'));
});

gulp.task('scss', function() {
    gulp.src('resources/sass/style.scss')
        .pipe(sourcemaps.init())
        .pipe(SASS({outputStyle: 'compressed'}))
        .on('error', swallowError)
        .pipe(rename('styles.min.css'))
        .on('error', swallowError)
        .pipe(sourcemaps.write('maps'))
        .on('error', swallowError)
        .pipe(gulp.dest('assets/css/'))
        .pipe(browserSync.reload({ stream: true}));

    gulp.src('resources/sass/editor-styles.scss')
        .pipe(sourcemaps.init())
        .pipe(SASS({outputStyle: 'compressed'}))
        .on('error', swallowError)
        .pipe(rename('editor-styles.min.css'))
        .on('error', swallowError)
        .pipe(sourcemaps.write('maps'))
        .on('error', swallowError)
        .pipe(gulp.dest('assets/css/'))
        .pipe(browserSync.reload({ stream: true}));
});

gulp.task('babel', function() {
    gulp.src(['resources/js/classes/*.js', 'resources/js/*.js'])
        .pipe(concat('concat.js'))
        .on('error', swallowError)
        .pipe(babel({
            presets: ['es2015']
        }))
        .on('error', swallowError)
        .pipe(uglify())
        .on('error', swallowError)
        .pipe(rename('main.min.js'))
        .pipe(gulp.dest('assets/js/'));
});

gulp.task('lint', function() {
    gulp.src('resources/js/main.js')
        .pipe(eslint({
            extends: 'eslint:recommended',
          ecmaFeatures: {
            'modules': true,
            'jsx': true
          },
          rules: {
            'no-unused-vars': 0,
            'no-undef': 0,
            'no-console': 0,
            'no-redeclare': 0
          },
          envs: ['browser', 'node', 'es6', 'jquery']
        }))
        .on('error', swallowError)
        .pipe(eslint.format());
});


gulp.task('imagemin', function() {
    gulp.src('assets/images/*')
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        }))
        .on('error', swallowError)
        .pipe(gulp.dest('assets/images/dist/'));
});

gulp.task('serve', ['scss'], function() {
    browserSync.init({
        open: 'external',
        host: 'eximia.local',
        proxy: 'eximia.local',
    });
});

gulp.task('watch', function() {
    gulp.watch(['resources/js/vendor/*.js', 'resources/js/legacy/*.js', 'resources/sass/vendor/*.css'], ['concat']);
    gulp.watch(['resources/js/classes/*.js', 'resources/js/main.js'], ['babel']).on('change', browserSync.reload);
    gulp.watch('resources/sass/**/*.scss', ['scss']);
});

gulp.task('default', ['concat', 'scss', 'lint', 'babel', 'watch', 'serve']);

function swallowError(error) {
    console.log(error.toString());
    try {
        notifier.notify({
            'title': 'Error:',
          'message': error.formatted.toString().trim().replace(/Error:/g, '')
        });
    } catch (e) {
        notifier.notify({
            'title': 'Error:',
          'message': error.toString()
        });
    }
    this.emit('end');
}


$( "#callus" ).click(function() {
	if ( $( "#cform" ).is( ":hidden" ) ) {
      $( "#cform" ).slideDown( "slow" );  
      $(".arrow").css("transform", "rotate(-180deg)");
  } else {
    $( "#cform" ).slideUp("slow");
    $(".arrow").css("transform", "rotate(0deg)");
  }
});
$( "#showsub .tab" ).click(function() {
	if ( $( "#hiddensub" ).is( ":hidden" ) ) {
      $( "#hiddensub" ).slideDown( "slow" );  
      $(".fl").css("transform", "rotate(-180deg)");
  } else {
    $( "#hiddensub" ).slideUp();
    $(".fl").css("transform", "rotate(0deg)");
  }
});

$( "#showcon .tab" ).click(function() {
	if ( $( "#hiddencon" ).is( ":hidden" ) ) {
      $( "#hiddencon" ).slideDown( "slow" );  
      $(".fl").css("transform", "rotate(-180deg)");
  } else {
    $( "#hiddencon" ).slideUp();
    $(".fl").css("transform", "rotate(0deg)");
  }
});
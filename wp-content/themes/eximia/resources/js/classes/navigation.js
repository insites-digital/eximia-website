
        /*
        |--------------------------------------------------------------------------
        | Menu Button
        |--------------------------------------------------------------------------
        |
        */
       $('.js-menu-button').click(function(e) {
        e.preventDefault();

        $(this).toggleClass('is-active');
        $('html').toggleClass('nav-active');
    });

    $('.dropdown-toggle').click(function(e) {
        e.preventDefault();

        if (jQuery.browser.mobile || $('html').hasClass('touchevents') || $(window).width() <= 980) {
            $(this).parent('.has-dropdown').toggleClass('dd-is-active');
        }
    });

    $('.nav-angle__bar.right').css('border-right-width', $( document ).width() / 2);
    $('.nav-angle__bar.left').css('border-left-width', $( document ).width() / 2);

    $(window).scroll(function() {    
        var scroll = $(window).scrollTop();
    
        if (scroll >= 20) {
            $(".c-site-nav").addClass("scrolled");
        } else {
            $(".c-site-nav").removeClass("scrolled");
        }
    });
/**
 * Detects if the user is using Internet Explorer and which version.
 * Adds the current version of IE if any, to the <html> tag.
 */
class DetectIE {
  /**
   * Construct our Detection instance.
   */
  constructor() {
    this.version = false;
    var ua = window.navigator.userAgent;
    if (!/MSIE/.test(ua) && !/rv:11.0/.test(ua) && !/Edge/.test(ua)) {
      return false;
    } else {
      var classString, msie, rv, edge;

      classString = "";
      msie = ua.indexOf("MSIE ");
      rv = ua.indexOf("rv:11.0");
      edge = ua.indexOf("Edge");

      if (msie > 0 || rv > 0) {
        classString += "any-ie";
      }
      if (rv > 0) {
        this.version = 11;
      }
      if (msie > 0) {
        this.version = ua.substring(msie + 4, msie + 7);
        this.version = this.version.replace(/ /g, '').replace(/\./g, '');
      }
      if (edge > 0) {
        this.version = "edge";
      }

      classString += " ie-" + this.version;
      document.documentElement.className += document.documentElement.className + " " + classString;
    }
  }

  /**
   * Return the version of IE.
   */
  getVersion() {
    return parseInt(this.version);
  }
}
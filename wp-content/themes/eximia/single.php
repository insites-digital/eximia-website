<?php 
get_header();
?>


<div class="hero-banner cstudy diff-padd textleft" data-equalizer>
    <div class="row expanded" >
        <div class="small-12 medium-12 large-4 large-offset-2 large-pull-1 columns padme textcol" data-equalizer-watch>
           <h3><?php echo the_title(); ?></h3>
                <p><?php echo get_the_date(); ?></p>
                <p>By <?php echo get_the_author(); ?></p>
        </div>
        <div class="small-12 medium-12 large-6 columns no-padding" data-equalizer-watch>
            <?php echo the_post_thumbnail('new-blog-image'); ?>
        </div>
    </div>
</div>

    

    <div class="social-sharing right">
        <h4>Share</h4>
        
        <!-- Sharingbutton Facebook -->
        <a class="resp-sharing-button__link" onClick="MyWindow=window.open('https://facebook.com/sharer/sharer.php?u=<?php echo the_permalink(); ?>','MyWindow','width=600,height=300'); return false;" href="https://facebook.com/sharer/sharer.php?u=<?php echo the_permalink(); ?>" target="popup" rel="noopener" aria-label="">
        <div class="resp-sharing-button resp-sharing-button--small">
            <div aria-hidden="true">
                <img alt="Facebook" title="Facebook" src="<?php echo home_url(); ?>/wp-content/uploads/2018/12/custom_iconfacebook_skin.png" width="24" height="24" style="" effect="">

            </div>
        </div>
        </a>

        <!-- Sharingbutton Twitter -->
        <a class="resp-sharing-button__link" onClick="MyWindow=window.open('https://twitter.com/intent/tweet/?text=Share&amp;url=<?php echo the_permalink(); ?>','MyWindow','width=600,height=300'); return false;" href="https://twitter.com/intent/tweet/?text=Share&amp;url=<?php echo the_permalink(); ?>" target="popup" rel="noopener" aria-label="">
        <div class="resp-sharing-button resp-sharing-button--small">
            <div aria-hidden="true" >
                <img alt="Twitter" title="Twitter" src="<?php echo home_url(); ?>/wp-content/uploads/2019/01/custom_icontwitter_skin.png" width="24" height="24" style="" effect="">
            </div>
        </div>
        </a>

        <!-- Sharingbutton Tumblr -->
        <a class="resp-sharing-button__link" onClick="MyWindow=window.open('https://www.tumblr.com/widgets/share/tool?posttype=link&amp;title=Share&amp;caption=Share&amp;content=<?php echo the_permalink(); ?>&amp;canonicalUrl=<?php echo the_permalink(); ?>&amp;shareSource=tumblr_share_button','MyWindow','width=600,height=300'); return false;" href="https://www.tumblr.com/widgets/share/tool?posttype=link&amp;title=Share&amp;caption=Share&amp;content=<?php echo the_permalink(); ?>&amp;canonicalUrl=<?php echo the_permalink(); ?>&amp;shareSource=tumblr_share_button" target="popup" rel="noopener" aria-label="">
        <div class="resp-sharing-button resp-sharing-button--small">
            <div aria-hidden="true">
                <img alt="LinkedIn" title="LinkedIn" src="<?php echo home_url(); ?>/wp-content/uploads/2018/12/custom_iconlinkedin_skin.png" width="24" height="24" style="" effect="">
            </div>
        </div>
        </a>
    </div>
    
     <div class="row single-post-content">
        <div class="columns">
            <?php echo the_content(); ?>
        </div>
    </div>

    <?php get_template_part('/partials/flexible-content-blog'); ?>

     <div class="row auth-details">
       <div class="columns padme" data-equalizer-watch>
	        <div class="author-img">
            <?php echo get_avatar(get_the_author_meta('ID'), 250); ?>
	        </div>
	        <div class="author-sup">
            <p><?php echo get_the_author(); ?></p>
            <p><?php echo the_author_meta('user_description'); ?></p>
            <a href="<?php echo get_author_posts_url($post->post_author); ?>" class="button">Explore more insights</a>
	        </div>
	        
	        
	     </div>
     </div>
		
     <div class="row social-details">
	     <div class="columns padme">
	        <div class="social-sharing mobile">
		        <h4>Share</h4>
		        
		        <!-- Sharingbutton Facebook -->
		        <a class="resp-sharing-button__link" onClick="MyWindow=window.open('https://facebook.com/sharer/sharer.php?u=<?php echo the_permalink(); ?>','MyWindow','width=600,height=300'); return false;" href="https://facebook.com/sharer/sharer.php?u=<?php echo the_permalink(); ?>" target="popup" rel="noopener" aria-label="">
			        <div class="resp-sharing-button resp-sharing-button--small">
			          <div aria-hidden="true">
			            <img alt="Facebook" title="Facebook" src="<?php echo home_url(); ?>/wp-content/uploads/2018/12/custom_iconfacebook_skin.png" width="24" height="24" style="" effect="">
			          </div>
			        </div>
		        </a>
		
		        <!-- Sharingbutton Twitter -->
		        <a class="resp-sharing-button__link" onClick="MyWindow=window.open('https://twitter.com/intent/tweet/?text=Share&amp;url=<?php echo the_permalink(); ?>','MyWindow','width=600,height=300'); return false;" href="https://twitter.com/intent/tweet/?text=Share&amp;url=<?php echo the_permalink(); ?>" target="popup" rel="noopener" aria-label="">
			        <div class="resp-sharing-button resp-sharing-button--small">
			          <div aria-hidden="true" >
			            <img alt="Twitter" title="Twitter" src="<?php echo home_url(); ?>/wp-content/uploads/2019/01/custom_icontwitter_skin.png" width="24" height="24" style="" effect="">
			          </div>
			        </div>
		        </a>
		
		        <!-- Sharingbutton Tumblr -->
		        <a class="resp-sharing-button__link" onClick="MyWindow=window.open('https://www.tumblr.com/widgets/share/tool?posttype=link&amp;title=Share&amp;caption=Share&amp;content=<?php echo the_permalink(); ?>&amp;canonicalUrl=<?php echo the_permalink(); ?>&amp;shareSource=tumblr_share_button','MyWindow','width=600,height=300'); return false;" href="https://www.tumblr.com/widgets/share/tool?posttype=link&amp;title=Share&amp;caption=Share&amp;content=<?php echo the_permalink(); ?>&amp;canonicalUrl=<?php echo the_permalink(); ?>&amp;shareSource=tumblr_share_button" target="popup" rel="noopener" aria-label="">
			        <div class="resp-sharing-button resp-sharing-button--small">
			          <div aria-hidden="true">
			            <img alt="LinkedIn" title="LinkedIn" src="<?php echo home_url(); ?>/wp-content/uploads/2018/12/custom_iconlinkedin_skin.png" width="24" height="24" style="" effect="">
			          </div>
			        </div>
		        </a>
			    </div>
		    </div>
    </div>

    <div class="row expanded">
        <div class="small-12 medium-6 columns">
            <div class="dropdown-tab" id="showcon">
	          	<div class="tab">
                <h4><i class="fa fa-phone"></i> Get in touch</h4>
                <div class="right">
	                <i class="fl fa fa-caret-down" aria-hidden="true"></i>
                </div>
	          	</div>
              <div class="hidden-area" id="hiddencon">
                <h5>We’d love to talk to you so we can find out more about you, your challenges and how we can put our expertise to good use to combat these together.<br>Please get in touch for an informal chat:</h5>
                <h6><a href="tel:+442074201984">+44 (0)20 7420 1984</a></h6>
                <h6><a href="mailto:hi@eximiacomms.co.uk">hi@eximiacomms.co.uk</a></h6>
              </div>
            </div>
        </div>
        <div class="small-12 medium-6 columns">
            <div class="dropdown-tab" id="showsub">
		          	<div class="tab">
	                <h4><i class="fa fa-envelope"></i> Subscribe for content updates</h4>
	                <div class="right">
	                    <i class="fl fa fa-caret-down" aria-hidden="true"></i>
	                </div>
		          	</div>
                <div class="hidden-area" id="hiddensub">
                    <div id="mc_embed_signup">
                        <?php get_template_part('partials/mailchimp-form'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row expanded related-posts">
        <div class="small-12 columns">
            <h2>You might also like</h2>
        </div>
    </div>

     <div class="row expanded" data-equalizer="foo">
        <div class="blog-posts">
            <h3><?php echo the_sub_field('section_title'); ?></h3>
            <?php 
            $currentID = get_the_ID();
            $args = array(
                'post_type' => 'post',
                'posts_per_page' => 3,
                'post__not_in' => array($currentID)
            );
            $the_query = new WP_Query($args);
            ?>

            <?php 
            if ($the_query->have_posts()) :
                while ($the_query->have_posts()) :

                $the_query->the_post();
            $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'blog-listing');
            ?>
            <div class="small-12 medium-4 large-4 columns wow fadeIn">
                <div class="blog-post" data-equalizer-watch="foo">
                    <img src="<?php echo $image[0]; ?>" alt="">
                    <h4><a href="<?php echo the_permalink(); ?>"><?php echo the_title(); ?></a></h4>
                </div>
            </div>
            <?php endwhile; ?> 
            <?php endif; ?>
            <?php wp_reset_query(); ?>
        </div>
    </div>

        <div class="row readmoreblogs">
      <div class="columns small-8 medium-6 large-3 small-centered wow fadeIn">
          <a href="<?php echo get_permalink(get_option('page_for_posts')); ?>" class="button all-blogs">Read more</a>
        </div>
    </div>

<?php get_footer(); ?>
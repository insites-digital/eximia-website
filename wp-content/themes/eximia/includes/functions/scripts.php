<?php

add_action('wp_enqueue_scripts', 'script_enqueues');

function script_enqueues() {
    //Remove default JQuery

    //Main scripts
    wp_enqueue_script('foundation', '//cdnjs.cloudflare.com/ajax/libs/foundation/6.4.3/js/foundation.min.js', array(), '6.4.3', true);    
    wp_enqueue_script('plugins', get_template_directory_uri() . '/assets/js/plugin.min.js', array('jquery'), '1.0.0', true);
    wp_enqueue_script('main', get_template_directory_uri() . '/assets/js/main.min.js', array('jquery'), '1.0.2', true);    
    wp_enqueue_style('vendor-styles', get_template_directory_uri() . '/assets/css/vendor.min.css', true, '1.0.0', 'all');
    wp_enqueue_style('main-styles', get_template_directory_uri() . '/assets/css/styles.min.css', true, '1.0.0', 'all');
}
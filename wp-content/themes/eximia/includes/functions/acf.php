<?php

if (function_exists('acf_add_options_page')) {
	acf_add_options_page(array(
		'page_title' 	=> 'General Theme Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
}


function my_acf_init() {
	acf_update_setting('google_api_key', 'AIzaSyBDmuAtj9XAqCF2B7b4ZwZsy9X0-AXcF8c');
}

add_action('acf/init', 'my_acf_init');
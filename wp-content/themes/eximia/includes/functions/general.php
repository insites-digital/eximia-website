<?php
	
/*******************************************************************************/
/* CONSTANTS
/*******************************************************************************/

define('THEME_DIRECTORY', get_template_directory());

/*
Body classes
- add more classes to the body to enable more specific targeting if needed
*/
function ambrosite_body_class($classes) {$post_name_prefix = 'postname-';$page_name_prefix = 'pagename-';$single_term_prefix = 'single-';$single_parent_prefix = 'parent-';$category_parent_prefix = 'parent-category-';$term_parent_prefix = 'parent-term-';$site_prefix = 'site-';global $wp_query;if ( is_single() ) {$wp_query->post = $wp_query->posts[0];setup_postdata($wp_query->post);$classes[] = $post_name_prefix . $wp_query->post->post_name;$taxonomies = array_filter( get_post_taxonomies($wp_query->post->ID), "is_taxonomy_hierarchical" );foreach ( $taxonomies as $taxonomy ) {$tax_name = ( $taxonomy != 'category') ? $taxonomy . '-' : '';$terms = get_the_terms($wp_query->post->ID, $taxonomy);if ( $terms ) {foreach( $terms as $term ) {if ( !empty($term->slug ) )$classes[] = $single_term_prefix . $tax_name . sanitize_html_class($term->slug, $term->term_id);while ( $term->parent ) {$term = &get_term( (int) $term->parent, $taxonomy );if ( !empty( $term->slug ) )$classes[] = $single_parent_prefix . $tax_name . sanitize_html_class($term->slug, $term->term_id);}}}}} elseif ( is_archive() ) {if ( is_category() ) {$cat = $wp_query->get_queried_object();while ( $cat->parent ) {$cat = &get_category( (int) $cat->parent);if ( !empty( $cat->slug ) )$classes[] = $category_parent_prefix . sanitize_html_class($cat->slug, $cat->cat_ID);}} elseif ( is_tax() ) {$term = $wp_query->get_queried_object();while ( $term->parent ) {$term = &get_term( (int) $term->parent, $term->taxonomy );if ( !empty( $term->slug ) )$classes[] = $term_parent_prefix . sanitize_html_class($term->slug, $term->term_id);}}} elseif ( is_page() ) {$wp_query->post = $wp_query->posts[0];setup_postdata($wp_query->post);$classes[] = $page_name_prefix . $wp_query->post->post_name;}if ( is_multisite() ) {global $blog_id;$classes[] = $site_prefix . $blog_id;}return $classes;} add_filter('body_class', 'ambrosite_body_class');

/*
Disable the theme editor
- stop clients from breaking their website
*/
define('DISALLOW_FILE_EDIT', true);

/*
Remove version info
- makes it that little bit harder for hackers
*/
function complete_version_removal() {
    return '';
}
add_filter('the_generator', 'complete_version_removal');

// Remove Wordpress default generators
remove_action('wp_head', 'wp_generator');

/*
	Disable XML-RPC
*/
add_filter('xmlrpc_enabled', '__return_false');

// Remove admin bar margin
add_action('get_header', 'my_filter_head');

// Remove image compression
add_filter('jpeg_quality', function($arg){return 100;});

function my_filter_head() {
    remove_action('wp_head', '_admin_bar_bump_cb');
}

//Remove Emoji JS for performance increase
function disable_wp_emojicons() {

  // all actions related to emojis
  remove_action( 'admin_print_styles', 'print_emoji_styles' );
  remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
  remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
  remove_action( 'wp_print_styles', 'print_emoji_styles' );
  remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
  remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
  remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );

  // filter to remove TinyMCE emojis
  add_filter( 'tiny_mce_plugins', 'disable_emojicons_tinymce' );
}

add_action( 'init', 'disable_wp_emojicons' );

function disable_emojicons_tinymce( $plugins ) {
  if ( is_array( $plugins ) ) {
    return array_diff( $plugins, array( 'wpemoji' ) );
  } else {
    return array();
  }
}

//remove WP REST API header links
remove_action( 'wp_head',      'rest_output_link_wp_head'              );
remove_action( 'wp_head',      'wp_oembed_add_discovery_links'         );
remove_action( 'template_redirect', 'rest_output_link_header', 11, 0 );

// Disable RSS feed
function itsme_disable_feed() {
wp_die( __( 'No feed available, please visit the <a href="'. esc_url( home_url( '/' ) ) .'">homepage</a>!' ) );
}

add_action('do_feed', 'itsme_disable_feed', 1);
add_action('do_feed_rdf', 'itsme_disable_feed', 1);
add_action('do_feed_rss', 'itsme_disable_feed', 1);
add_action('do_feed_rss2', 'itsme_disable_feed', 1);
add_action('do_feed_atom', 'itsme_disable_feed', 1);
add_action('do_feed_rss2_comments', 'itsme_disable_feed', 1);
add_action('do_feed_atom_comments', 'itsme_disable_feed', 1);

remove_action( 'wp_head', 'feed_links_extra', 3 );
remove_action( 'wp_head', 'feed_links', 2 );

// Remove query strings from static resources
function _remove_script_version( $src ){ 
	$parts = explode( '?', $src ); 
	return $parts[0]; 
} 
add_filter( 'script_loader_src', '_remove_script_version', 15, 1 ); 
add_filter( 'style_loader_src', '_remove_script_version', 15, 1 );

function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

/**
 * admin AJAX function example
 * add_action('wp_ajax_example_admin_ajax', 'example_admin_ajax');
 * add_action('wp_ajax_nopriv_example_admin_ajax', 'example_admin_ajax');
 */

/*******************************************************************************/
/* FILTERS
/*******************************************************************************/

//add_filter('wp_footer', 'js_theme_uri');

/*******************************************************************************/
/* THEME SUPPORTS
/*******************************************************************************/

$markup = array('search-form', 'comment-form', 'comment-list');

add_theme_support('title-tag');
add_theme_support('post-thumbnails');
add_theme_support('html5', $markup);

add_post_type_support( 'post', 'page-attributes' );

add_editor_style( 'css/editor-styles.min.css' );

/*******************************************************************************/
/* SIDEBARS
/*******************************************************************************/

/*register_sidebar( array(
        'name' => __( 'Main Sidebar', 'theme-slug' ),
        'id' => 'sidebar-1',
        'description' => __( 'Widgets in this area will be shown on all posts and pages.', 'theme-slug' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
	'after_widget'  => '</li>',
	'before_title'  => '<h2 class="widgettitle">',
	'after_title'   => '</h2>',
) );*/
    
/*******************************************************************************/
/* NAVIGATION MENUS
/*******************************************************************************/

$nav_menus = array(
  'main' => 'Main Navigation',
  'policies' => 'Policies Footer',
  'footerwidget' => 'Widget Footer'
);
register_nav_menus($nav_menus);


/*******************************************************************************/
/* IMAGE SIZES
/*******************************************************************************/

add_image_size('client-logo', 200, 100, false);
add_image_size('blogpost', 640, 540);
add_image_size('new-blog-image', 1136, 540, true);
add_image_size('blog-listing', 586, 423, true);
add_image_size('avatar', 200, 200, true);
//add_image_size('banner-image', 1800, 600, array( 'center', 'center' ));
//add_image_size('blog-teaser', 350, 250, true);

/*******************************************************************************/
/* OTHER
/*******************************************************************************/

if (!isset($content_width)) {
	$content_width = 1000;
}

function site_logo($format, $height, $width) {
  $siteName = get_bloginfo('name');
	$logo = '';
  $logoUrl = get_template_directory_uri() . '/assets/images/logo.' . $format;
  
  if ($height):
    $height = 'height="' . $height . '"';
  endif;

  if ($width):
    $width = 'width="' . $width . '"';
  endif;

	
	if ($format != '') {
    $logo = '<img class="logo" src="' . $logoUrl . '" alt="' . $siteName . '" ' . $height . ' ' . $width . ' />';
	} else {
		$logo = '<div class="logo">' . $siteName . '</div>';
	}
	echo $logo;
}

function js_theme_uri() {
	if (!is_404()) {
		//echo '<script>window.theme_uri = "' . get_template_directory_uri() . '";</script>';
	}
}

/**
 * Excerpt changes
 */
function custom_excerpt_length( $length ) {
    return 20;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

function custom_excerpt_more( $more ) {
    return '...';
}
add_filter( 'excerpt_more', 'custom_excerpt_more' );

function limit_words($words, $limit, $append = ' &hellip;') {
       // Add 1 to the specified limit becuase arrays start at 0
       $limit = $limit+1;
       // Store each individual word as an array element
       // Up to the limit
       $words = explode(' ', $words, $limit);
       // Shorten the array by 1 because that final element will be the sum of all the words after the limit
       array_pop($words);
       // Implode the array for output, and append an ellipse
       $words = implode(' ', $words) . $append;
       // Return the result
       return $words;
}

/**
 * Menu pagination
 */
if ( ! function_exists( 'the_prev_menu' ) ) :
  function the_prev_menu() {
    $prev_post = get_adjacent_post(false, '', true);

    if ( is_a( $prev_post, 'WP_Post' ) ) {
      $servingHours = get_field('serving_hours', $prev_post->ID);

      echo '<div class="menu-link left"><a class="prev-post" href="' . get_permalink( $prev_post->ID ) . '"><div class="menu-nav-heading">' . get_the_title( $prev_post->ID ) . '</div>'; 
      if ($servingHours): 
        foreach($servingHours as $servingHoursRow) { 
          echo '<div class="menu-nav-subheading">' . $servingHoursRow['serving_hour'] . '</div>'; 
        }
      endif;
      echo '</a></div>';
    } else {
      $postType = get_post_type( get_the_ID() );
      $first = new WP_Query('post_type=' . $postType . '&posts_per_page=1&order=DESC&orderby=menu_order'); $first->the_post();
      $servingHours = get_field('serving_hours');
      echo '<div class="menu-link left"><a class="prev-post" href="' . get_permalink() . '"><div class="menu-nav-heading">' . get_the_title() . '</div>';
      if ($servingHours): 
        foreach($servingHours as $servingHoursRow) { 
          echo '<div class="menu-nav-subheading">' . $servingHoursRow['serving_hour'] . '</div>'; 
        }
      endif;
      echo '</a></div>';
      wp_reset_query();
    }
  }
endif;

if ( ! function_exists( 'the_next_menu' ) ) :
  function the_next_menu() {
    $next_post = get_adjacent_post(false, '', false);

    if ( is_a( $next_post, 'WP_Post' ) ) {
      $servingHours = get_field('serving_hours', $next_post->ID);

      echo '<div class="menu-link right"><a class="next-post" href="' . get_permalink( $next_post->ID ) . '"><div class="menu-nav-heading">' . get_the_title( $next_post->ID ) . '</div>'; 
      if ($servingHours): 
        foreach($servingHours as $servingHoursRow) { 
          echo '<div class="menu-nav-subheading">' . $servingHoursRow['serving_hour'] . '</div>'; 
        }
      endif;
      echo '</a></div>';
    } else {
      $postType = get_post_type( get_the_ID() );
      $last = new WP_Query('post_type=' . $postType . '&posts_per_page=1&order=ASC'); $last->the_post();
      $servingHours = get_field('serving_hours');
      echo '<div class="menu-link right"><a class="next-post" href="' . get_permalink() . '"><div class="menu-nav-heading">' . get_the_title() . '</div>';
      if ($servingHours): 
        foreach($servingHours as $servingHoursRow) { 
          echo '<div class="menu-nav-subheading">' . $servingHoursRow['serving_hour'] . '</div>'; 
        }
      endif;
      echo '</a></div>';
      wp_reset_query();
    }
  }
endif;

if ( ! function_exists( 'close_menu' ) ) :
  function close_menu() {
    echo '<a class="close-menu" href="/our-menu/"></a>';
  }
endif;

// Opening times
if ( ! function_exists( 'current_open_times' ) ) :
  function current_open_times() {
    $currentDay = date('l');
    $currentDate = date('l jS F');
    $weekdays = ['Tuesday','Wednesday','Thursday','Friday', 'Saturday'];
    $weekends = ['Sunday'];
    $currentTimes = '';

    if (in_array($currentDay, $weekdays)):
      $currentTimes = 'Open 10am - 11pm';
    elseif (in_array($currentDay, $weekends)):
      $currentTimes = 'Open 10am - 6pm';
    else: 
      $currentTimes = 'Closed';
    endif;

    if ($currentTimes != ''):
      echo '<p>' . $currentDate . '</p>';
      echo '<p>' . $currentTimes . '</p>';
    endif;
  }
endif;

// DKPDF Fonts
add_filter( 'dkpdf_mpdf_font_dir', function ( $font_dir ) {
	// path to wp-content directory
	$wp_content_dir = trailingslashit( WP_CONTENT_DIR );
	array_push( $font_dir, $wp_content_dir . 'fonts' );
	return $font_dir;
});
/**
 * Define the font details in fontdata configuration variable
 */
add_filter( 'dkpdf_mpdf_font_data', function( $font_data ) {
	$font_data['montserrat'] = [
    'R' => 'Montserrat-Regular.ttf',
    'B' => 'Montserrat-SemiBold.ttf',
	];
	return $font_data;
});


function eximia_widgets_init() {

	register_sidebar( array(
		'name'          => 'Footer Column 1',
		'id'            => 'footer-column-1',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '<h5>',
		'after_title'   => '</h5>',
	) );
	register_sidebar( array(
		'name'          => 'Footer Column 2',
		'id'            => 'footer-column-2',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '<h5>',
		'after_title'   => '</h5>',
	) );
	register_sidebar( array(
		'name'          => 'Footer Column 3',
		'id'            => 'footer-column-3',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '<h5>',
		'after_title'   => '</h5>',
	) );
	register_sidebar( array(
		'name'          => 'Footer Column 4',
		'id'            => 'footer-column-4',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '<h5>',
		'after_title'   => '</h5>',
	) );

}
add_action( 'widgets_init', 'eximia_widgets_init' );

function oembed_change_size($embed_size){
	$embed_size['width'] = 100;
	$embed_size['height'] = 500;
	return $embed_size;
}
add_filter('embed_defaults', 'oembed_change_size');


/* Scale up images functionality in "Edit image" ...
 * See http://core.trac.wordpress.org/ticket/23713
 * This is slightly changed function image_resize_dimensions()
in wp-icludes/media.php */
function my_image_resize_dimensions($nonsense, $orig_w, $orig_h, $dest_w, $dest_h, $crop = false)
{

  if ($crop) {
        // crop the largest possible portion of the original image that we can size to $dest_w x $dest_h
    $aspect_ratio = $orig_w / $orig_h;
    $new_w = min($dest_w, $orig_w);
    $new_h = min($dest_h, $orig_h);

    if (!$new_w) {
      $new_w = intval($new_h * $aspect_ratio);
    }

    if (!$new_h) {
      $new_h = intval($new_w / $aspect_ratio);
    }

    $size_ratio = max($new_w / $orig_w, $new_h / $orig_h);

    $crop_w = round($new_w / $size_ratio);
    $crop_h = round($new_h / $size_ratio);

    $s_x = floor(($orig_w - $crop_w) / 2);
    $s_y = floor(($orig_h - $crop_h) / 2);
  } else {
        // don't crop, just resize using $dest_w x $dest_h as a maximum bounding box
    $crop_w = $orig_w;
    $crop_h = $orig_h;

    $s_x = 0;
    $s_y = 0;

        /* wp_constrain_dimensions() doesn't consider higher values for $dest :( */
        /* So just use that function only for scaling down ... */
    if ($orig_w >= $dest_w && $orig_h >= $dest_h) {
      list($new_w, $new_h) = wp_constrain_dimensions($orig_w, $orig_h, $dest_w, $dest_h);
    } else {
      $ratio = $dest_w / $orig_w;
      $w = intval($orig_w * $ratio);
      $h = intval($orig_h * $ratio);
      list($new_w, $new_h) = array($w, $h);
    }
  }

    // if the resulting image would be the same size or larger we don't want to resize it
    // Now WE need larger images ...
    //if ( $new_w >= $orig_w && $new_h >= $orig_h )
  if ($new_w == $orig_w && $new_h == $orig_h)
    return false;

    // the return array matches the parameters to imagecopyresampled()
    // int dst_x, int dst_y, int src_x, int src_y, int dst_w, int dst_h, int src_w, int src_h
  return array(0, 0, (int)$s_x, (int)$s_y, (int)$new_w, (int)$new_h, (int)$crop_w, (int)$crop_h);

}
add_filter('image_resize_dimensions', 'my_image_resize_dimensions', 1, 6);
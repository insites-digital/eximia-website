<?php

function create_posttypes() {
 
    register_post_type( 'accreditations',
        array(
            'labels' => array(
                'name' => __( 'Accreditations' ),
                'singular_name' => __( 'Accreditation' )
            ),
            'public' => true,
            'has_archive' => false,
            'rewrite' => array('slug' => 'accreditations'),
            'menu_icon'   => 'dashicons-media-archive',
            'supports' => array( 'title', 'thumbnail')
        )
    );
 
    register_post_type( 'clients',
        array(
            'labels' => array(
                'name' => __( 'Clients' ),
                'singular_name' => __( 'Client' )
            ),
            'public' => true,
            'has_archive' => false,
            'rewrite' => array('slug' => 'clients'),
            'menu_icon'   => 'dashicons-admin-users',
            'supports' => array( 'title', 'thumbnail')
        )
    );
    
    register_post_type( 'testimonials',
        array(
            'labels' => array(
                'name' => __( 'Testimonials' ),
                'singular_name' => __( 'Testimonial' )
            ),
            'public' => true,
            'has_archive' => false,
            'rewrite' => array('slug' => 'testimonials'),
            'menu_icon'   => 'dashicons-clipboard',
            'supports' => array( 'title', 'thumbnail', 'editor')
        )
    );
    
    register_post_type( 'case-studies',
        array(
            'labels' => array(
                'name' => __( 'Case Studies' ),
                'singular_name' => __( 'Case Study' )
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'work'),
            'menu_icon'   => 'dashicons-portfolio',
            'supports' => array( 'title', 'thumbnail', 'editor')
        )
    );

}
// Hooking up our function to theme setup
add_action( 'init', 'create_posttypes' );

function work_categories() {  
    register_taxonomy(  
        'work_categories',  //The name of the taxonomy. Name should be in slug form (must not contain capital letters or spaces). 
        'case-studies',        //post type name
        array(  
            'hierarchical' => true,  
            'label' => 'Categories',  //Display name
            'query_var' => true,
            'rewrite' => array(
                'slug' => 'categories', // This controls the base slug that will display before each term
                'with_front' => false // Don't display the category base before 
            )
        )  
    );  
}  
add_action( 'init', 'work_categories');